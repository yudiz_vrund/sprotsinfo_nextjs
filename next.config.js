const nextTranslate = require('next-translate');
const withPWA = require('next-pwa');

module.exports = withPWA({
  ...nextTranslate(),
  images: {
    domains: ['image.crictracker.com'],
  },
  amp: {
    validator: './custom_validator.js',
  },
  pwa: {
    dest: 'public',
    disable: process.env.NODE_ENV == 'development',
  },
});
