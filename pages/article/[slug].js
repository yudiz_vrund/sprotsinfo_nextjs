/* eslint-disable @next/next/no-img-element */
import React from 'react';
import Layout from '../../Components/Layout/Layout';
import Carasoul from '../../Components/Carasoul/Carasoul';
import CarasoulAmp from '../../Components/Carasoul/CarasoulAmp';
import styles from '../../Components/Carasoul/Carasoul.module.css';
import parse, { attributesToProps } from 'html-react-parser';
import PropTypes from 'prop-types';
import { useAmp } from 'next/amp';
import LayoutAmp from '../../Components/Layout/LayoutAmp';
import { Element } from 'domhandler/lib/node';

export const config = { amp: 'hybrid' };
export default function Article({ FinalData }) {
  const isAmp = useAmp();

  var today = new Date();
  var today_month = today.getMonth();
  var date = new Date(FinalData.dCreatedAt);
  var month = date.getMonth();
  var final_month = today_month - month;
  return (
    <>
      <style jsx>{`
        .mainDes {
          font-family: 'IBM Plex Sans Condensed', sans-serif;
          font-weight: 400;
          padding: 60px 30px;
          margin: -40px 0 -20px 420px;
          max-width: 960px;
          font-size: 18px;
          text-align: left;
          line-height: 28px;
          color: #3d3d3d;
        }

        .mainDes > span {
          color: #3d3d3d;
        }

        .mainDes .description > h3 {
          color: #292929;
        }

        .description strong {
          font-weight: bolder;
        }

        .description h2,
        .description h3,
        .description h4 {
          font-size: 20px;
          line-height: 25px;
          font-family: 'IBM Plex Sans', sans-serif;
          font-weight: 800;
          color: #292929;
        }
        @media (max-width: 1440px) {
          .mainDes {
            margin: -40px 0 -20px 120px;
          }
        }

        @media (max-width: 1024px) {
          .mainDes {
            margin: -40px 0 -20px 20px;
            font-size: 16px;
            line-height: 26px;
          }

          .description p span {
            font-size: 16px;
            line-height: 26px;
          }
          .description h2,
          .description h3,
          .description h4 {
            font-size: 18px;
            line-height: 22px;
            font-family: 'IBM Plex Sans', sans-serif;
            font-weight: 800;
            color: #292929;
          }
        }

        @media (max-width: 768px) {
          .mainDes {
            margin: -50px 0 -20px -10px;
            overflow-x: scroll;
          }

          .description p span {
            font-size: 16px;
            line-height: 26px;
          }

          .description span {
            font-family: 'IBM Plex Sans', sans-serif;
          }
          .description h2,
          .description h3,
          .description h4 {
            font-size: 18px;
            line-height: 22px;
            font-family: 'IBM Plex Sans', sans-serif;
            font-weight: 800;
            color: #292929;
          }
        }

        @media (max-width: 425px) {
          .mainDes {
            font-size: 14px;
            line-height: 22px;
          }

          .description {
            margin: -30px 0 -20px -10px;
            font-size: 14px;
            line-height: 20px;
            overflow-x: scroll;
            font-family: 'IBM Plex Sans', sans-serif;
            text-align: left;
          }
          .description p span {
            font-size: 14px;
            line-height: 22px;
          }
          .description h2,
          .description h3,
          .description h4 {
            font-size: 17px;
            line-height: 22px;
            font-family: 'IBM Plex Sans', sans-serif;
            font-weight: 800;
            color: #292929;
          }
        }

        @media (max-width: 375px) {
          .description {
            margin: -30px 0 -20px -10px;
            font-size: 14px;
            line-height: 20px;
            overflow-x: scroll;
            font-family: 'IBM Plex Sans', sans-serif;
            text-align: left;
          }

          .description p span {
            font-size: 14px;
            line-height: 22px;
          }
          .description h2,
          .description h3,
          .description h4 {
            font-size: 17px;
            line-height: 22px;
            font-family: 'IBM Plex Sans', sans-serif;
            font-weight: 800;
            color: #292929;
          }
        }

        @media (max-width: 320px) {
          .mainDes .aligncenter {
            width: 100%;
            height: 100%;
          }

          .description {
            margin-left: 0px;
            font-size: 14px;
            line-height: 20px;
            overflow-x: scroll;
            font-family: 'IBM Plex Sans', sans-serif;
          }
          .description {
            margin: -30px 0 -20px -10px;
            font-size: 14px;
            text-align: left;
          }

          .description p span {
            font-size: 14px;
            line-height: 22px;
          }

          .description h2,
          .description h3,
          .description h4 {
            font-family: 'IBM Plex Sans', sans-serif;
            font-size: 17px;
            line-height: 22px;
            font-weight: 800;
          }
        }
      `}</style>
      {isAmp ? (
        <LayoutAmp title={FinalData.sTitle}>
          <div style={{ backgroundColor: 'white' }}>
            <CarasoulAmp
              image={FinalData.sImage}
              title={FinalData.sTitle}
              comment={FinalData.nCommentsCount}
              view={FinalData.nViewCounts}
              month={final_month}
            />
            <div className="mainDes">
              {parse(FinalData.sDescription, {
                replace: (domNode) => {
                  if (domNode instanceof Element && domNode.name === 'img') {
                    const props = attributesToProps(domNode.attribs);
                    return (
                      <amp-img
                        width={props.width}
                        height={props.height}
                        layout="responsive"
                        src={props.src}
                      >
                        <noscript>
                          <img {...props} alt="sports"></img>
                        </noscript>
                      </amp-img>
                    );
                  }
                  if (domNode instanceof Element && domNode.name === 'script') {
                    return <></>;
                  }
                },
              })}
            </div>
          </div>
        </LayoutAmp>
      ) : (
        <Layout title={FinalData.sTitle}>
          <Carasoul
            image={FinalData.sImage}
            title={FinalData.sTitle}
            comment={FinalData.nCommentsCount}
            view={FinalData.nViewCounts}
            month={final_month}
          />
          <div className={styles.mainDes}>
            <p className={styles.description}>
              {parse(FinalData.sDescription)}
            </p>
          </div>
        </Layout>
      )}
    </>
  );
}

Article.propTypes = {
  FinalData: PropTypes.object,
};

export async function getServerSideProps({ query }) {
  const { slug } = query;

  const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/view/${slug}`);
  const data = await res.json();
  const FinalData = data.data;
  return {
    props: {
      FinalData,
    },
  };
}
