import { useState } from 'react';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import styles from '../Components/Layout/Layout.module.css';
import Router from 'next/router';
import PropTypes from 'prop-types';

function MyApp({ Component, pageProps }) {
  const [loader, setLoader] = useState(false);

  Router.onRouteChangeStart = (url) => {
    setLoader(true);
  };

  Router.onRouteChangeComplete = (url) => {
    setLoader(false);
  };

  return (
    <>
      {loader && (
        <div className={styles.red_loader}>
          <span></span>
        </div>
      )}
      {!loader && <Component {...pageProps} />}
    </>
  );
}

MyApp.propTypes = {
  Component: PropTypes.any,
  pageProps: PropTypes.any,
};

export default MyApp;
