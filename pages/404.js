import React from 'react';
import Link from 'next/link';
import { FaExclamationTriangle } from 'react-icons/fa';
import styles from '../styles/Error.module.css';
import Layout from '../Components/Layout/Layout';
import useTranslation from 'next-translate/useTranslation';

const ErrorPage = () => {
  let { t } = useTranslation();
  return (
    <Layout title="Page not Found">
      <div className={styles.error}>
        <h1>
          <FaExclamationTriangle /> {t('common:number')}
        </h1>
        <h4>{t('common:page')}</h4>
        <Link href="/">
          <a>{t('common:homepage')}</a>
        </Link>
      </div>
    </Layout>
  );
};

export default ErrorPage;
