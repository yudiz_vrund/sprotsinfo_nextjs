import React from 'react';
import Layout from '../Components/Layout/Layout';
import useTranslation from 'next-translate/useTranslation';
import styles from '../styles/About.module.css';

const About = () => {
  let { t } = useTranslation();

  return (
    <Layout title="About Page">
      <div className={styles.container}>
        <p className={styles.p1}>{t('about:About')}</p>
        <p className={styles.p2}>{t('about:para')}</p>
        <p className={styles.p2}>{t('about:para')}</p>
        <p className={styles.p2}>{t('about:para')}</p>
        <p className={styles.p2}>{t('about:para')}</p>
      </div>
    </Layout>
  );
};

export default About;
