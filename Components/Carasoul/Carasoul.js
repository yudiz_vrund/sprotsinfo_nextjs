/* eslint-disable @next/next/no-img-element */
import React from 'react';
import styles from '../Carasoul/Carasoul.module.css';
import useTranslation from 'next-translate/useTranslation';
import PropTypes from 'prop-types';

function Carasoul({ image, title, comment, view, month }) {
  let { t } = useTranslation();
  return (
    <>
      <div
        id="carouselExampleSlidesOnly myCarasoul"
        className="carousel slide"
        data-bs-ride="carousel"
      >
        <div className="carousel-inner">
          <div className="carousel-item active">
            <div
              className={styles.myCarasoul}
              style={{
                backgroundImage: `url(${image})`,
              }}
            >
              <div className={styles.inner_carasoul}>
                <div className="container">
                  <div className={styles.cara_container}>
                    <h1>{title}</h1>
                    <div className="d-flex justify-content-between align-items-center">
                      <p className={styles.author}>
                        <a>{t('common:title')}</a>
                        {month} {t('common:month')}
                      </p>

                      <ul className={styles.count_list}>
                        <li>
                          <p className={styles.comment_white_icon}>
                            <img
                              src={
                                'https://www.sports.info/comment-icon.7aef209a3b2086028430.svg'
                              }
                              alt="comment"
                            />
                            <span className={styles.space}>{comment}</span>
                          </p>
                        </li>
                        <li>
                          <p className={styles.comment_white_icon}>
                            <img
                              src={
                                'https://www.sports.info/view-icon.b16661e96527947b18f1.svg'
                              }
                              alt="eye"
                            />
                            <span className={styles.space}>{view}</span>
                          </p>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

Carasoul.propTypes = {
  image: PropTypes.any,
  title: PropTypes.string,
  comment: PropTypes.number,
  view: PropTypes.number,
  month: PropTypes.number,
};

export default Carasoul;
