/* eslint-disable @next/next/no-img-element */
import React from 'react';
import useTranslation from 'next-translate/useTranslation';
import PropTypes from 'prop-types';

function Carasoul({ image, title, comment, view, month }) {
  let { t } = useTranslation();
  return (
    <>
      <style jsx>{`
        .container {
          margin: 0 60px;
          padding: 0 12px;
        }

        .pic {
          margin-bottom: -5px;
          margin-right: 5px;
        }

        .myCarasoul {
          position: relative;
          overflow: hidden;
          height: 680px;
          margin-top: -6px;
          background: top center/cover no-repeat #1b2a5a;
        }

        .inner_carasoul {
          width: 100%;
          position: absolute;
          bottom: 0;
          z-index: 2;
          font-family: 'IBM Plex Sans', sans-serif;
          line-height: 26px;
          font-size: 16px;
          text-align: left;
        }

        .custom {
          display: flex;
          justify-content: space-between;
          align-items: center;
        }

        .mainDes {
          background-color: white;
        }

        .cara_container {
          padding: 32px 0 16px 88px;
          max-width: 72.6%;
          width: 852px;
          margin-left: 300px;
        }

        .myCarasoul::after {
          content: '';
          position: absolute;
          width: 100%;
          height: 50%;
          bottom: 0;
          background-image: linear-gradient(
            to bottom,
            rgba(27, 42, 90, 0),
            rgba(27, 42, 90, 1)
          );
          z-index: 1;
        }

        .cara_container h1 {
          margin: 0 0 24px;
          font-size: 48px;
          line-height: 64px;
          font-family: Muli, sans-serif;
          font-weight: 900;

          text-transform: uppercase;
          font-style: italic;
          color: white;
        }

        .author {
          color: #bec2cb;
          opacity: 1;
          font-size: 18px;
          line-height: 23px;
          margin: 0;
          font-family: 'IBM Plex Sans Condensed', sans-serif;
          font-weight: 400;
          display: inline-block;
        }
        .author a {
          color: #fff;
          margin-right: 4px;
          text-decoration: none;
          cursor: pointer;
          background-color: transparent;
          font-weight: 400;
          text-transform: uppercase;
          border-bottom: 2px solid transparent;
        }

        .count_list {
          margin: 0;
          flex-shrink: 0;
          list-style: none;
        }
        .count_list li {
          display: inline-block;
        }

        .comment_white_icon {
          color: #bcc2cf;
          font-size: 16px;
          margin-top: 10px;
        }

        .count_list p:before {
          content: '';
          margin-right: 4px;
          display: inline-block;
          width: 20px;
          height: 20px;
          background: center center/20px auto no-repeat;
          vertical-align: middle;
        }

        .space {
          padding-left: 0px;
        }

        @media (max-width: 1440px) {
          .count_list p:before {
            margin-right: -8px;
          }
          .mainDes {
            background-color: white;
          }
          .cara_container {
            margin-left: 0px;
          }
        }

        @media (max-width: 1024px) {
          .container {
            margin: 0 32px;
          }
          .count_list p:before {
            margin-right: -8px;
          }
          .cara_container h1 {
            margin: 0 0 8px;
            font-size: 36px;
            line-height: 50px;
          }
          .cara_container {
            padding: 32px 0 16px 0px;
            max-width: 90%;
          }
        }

        @media (max-width: 768px) {
          .container {
            margin: 0 24px;
          }
          .myCarasoul {
            height: 430px;
            margin-top: 30px;
          }

          .count_list p:before {
            margin-right: -8px;
          }
          .cara_container h1 {
            margin: 0 0 8px;
            font-size: 36px;
            line-height: 50px;
            max-width: 668px;
          }

          .custom {
            width: 668px;
          }

          .author {
            font-size: 14px;
            line-height: 20px;
          }
          .cara_container {
            padding: 32px 0 16px 0px;
            max-width: 90%;
          }
          .comment_white_icon {
            padding: 0;
            margin: 0;
            font-size: 12px;
          }
          .description {
            margin-left: 10px;
            overflow-x: scroll;
          }
          .author {
            font-size: 16px;
          }
          .space {
            font-size: 14px;
          }
        }

        @media (max-width: 425px) {
          .container {
            margin: 0 0;
          }
          .myCarasoul {
            height: 330px;
            margin-top: 20px;
          }
          .cara_container h1 {
            margin: 0 0 8px;
            font-size: 24px;
            line-height: 32px;
            width: 393px;
          }
          .count_list p:before {
            margin-right: -8px;
          }
          .custom {
            width: 393px;
          }
        }

        @media (max-width: 375px) {
          .myCarasoul {
            height: 330px;
            margin-top: 20px;
          }
          .cara_container h1 {
            margin: 0 0 8px;
            font-size: 24px;
            line-height: 32px;
            width: 344px;
          }
          .author {
            font-size: 12px;
            line-height: 20px;
          }

          .count_list p:before {
            margin-right: -8px;
          }

          .comment_white_icon {
            font-size: 12px;
          }
          .custom {
            width: 344px;
          }

          .space {
            padding-left: 2px;
          }
          .comment_white_icon {
            margin: 0;
          }
        }

        @media (max-width: 320px) {
          .myCarasoul {
            height: 350px;
          }
          .cara_container h1 {
            margin: 0 0 8px;
            font-size: 24px;
            line-height: 32px;
            width: 290px;
          }
          .author {
            font-size: 12px;
            line-height: 20px;
          }

          .comment_white_icon {
            font-size: 12px;
          }
          .custom {
            width: 290px;
          }
          .space {
            padding-left: 2px;
          }
          .comment_white_icon {
            margin: 0;
          }
          .count_list p:before {
            margin-right: -8px;
          }
        }
      `}</style>
      <div
        id="carouselExampleSlidesOnly myCarasoul"
        className="carousel slide"
        data-bs-ride="carousel"
      >
        <div className="carousel-inner">
          <div className="carousel-item active">
            <div
              className="myCarasoul"
              style={{
                backgroundImage: `url(${image})`,
              }}
            >
              <div className="inner_carasoul">
                <div className="container">
                  <div className="cara_container">
                    <h1>{title}</h1>
                    <div className="custom">
                      <p className="author">
                        <a>{t('common:title')}</a>
                        {month} {t('common:month')}
                      </p>

                      <ul className="count_list">
                        <li>
                          <p className="comment_white_icon">
                            <amp-img
                              src={
                                'https://www.sports.info/comment-icon.7aef209a3b2086028430.svg'
                              }
                              alt="comment"
                              width="20"
                              height="20"
                              className="pic"
                            />
                            <span className="space">{comment}</span>
                          </p>
                        </li>
                        <li>
                          <p className="comment_white_icon">
                            <amp-img
                              src={
                                'https://www.sports.info/view-icon.b16661e96527947b18f1.svg'
                              }
                              alt="eye"
                              width="20"
                              height="20"
                              className="pic"
                            />
                            <span className="space">{view}</span>
                          </p>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

Carasoul.propTypes = {
  image: PropTypes.any,
  title: PropTypes.string,
  comment: PropTypes.number,
  view: PropTypes.number,
  month: PropTypes.number,
};

export default Carasoul;
