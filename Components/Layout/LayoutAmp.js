import React from 'react';
import Head from 'next/head';
import FooterAmp from '../Footer/FooterAmp';
import PropTypes from 'prop-types';
import HeaderAmp from '../Header/HeaderAmp';

const Layout = ({ title, children }) => {
  return (
    <>
      <style jsx>{`
        .main {
          background-color: #f4f4f5;
          padding: 20px 0;
        }
      `}</style>
      <Head>
        <title>{title}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta property="og:title" content={title} key="title" />
        <meta name="theme-color" content="#317EFB" />
      </Head>
      <HeaderAmp />
      <div className="main">{children}</div>
      <FooterAmp />
    </>
  );
};

Layout.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node,
};

export default Layout;
