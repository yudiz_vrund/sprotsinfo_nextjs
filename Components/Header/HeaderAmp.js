/* eslint-disable @next/next/no-img-element */
import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

const Header = () => {
  let router = useRouter();
  return (
    <>
      <style jsx>{`
        .header {
          display: flex;
          justify-content: space-between;
          align-items: center;
          background: #22336f;
          height: 56px;
          padding: 0 20px;
          position: fixed;
          width: 100%;
          z-index: 5;
          font-family: 'IBM Plex Sans', sans-serif;
          margin: 0 -15px;
        }

        .header a {
          color: #333;
          margin-right: 20px;
        }

        .header a:hover {
          color: #fff;
        }
        .header ul {
          display: flex;
          align-items: center;
          justify-content: center;
          list-style: none;
          margin-top: 0px;
        }

        .header ul li a {
          text-decoration: none;
          color: #fff;
          opacity: 0.5;
        }

        .nav_bar {
          margin-right: 38px;
          margin-bottom: 0;
          padding: 3px 0 4px 0;
          display: inline-block;
          position: relative;
          z-index: 99;
        }
        .nav_bar::before {
          content: '';
          position: absolute;
          width: 200%;
          height: calc(100% + 0px);
          right: -38px;
          bottom: 0;
          background: #fff;
          transform: skewX(24deg) perspective(1200px);
          -webkit-transform: skewX(24deg) perspective(1200px);
          border-radius: 0 28px 0 0;
        }

        .sports_logo {
          border-radius: 0px 160px 0px 0px;
          margin: 0px 0px 0px -20px;
          padding: 3px 30px 3px 20px;
          position: relative;
          background: #fff;
          height: 50px;
          width: 200px;
        }
        .wologo {
          margin-top: 5px;
        }

        @media (max-width: 1440px) {
          .header {
            margin: 0 -10px;
          }
          .header ul {
            margin-top: 2px;
          }
        }

        @media (max-width: 1024px) {
          .header ul {
            margin-top: 5px;
          }
        }

        @media (max-width: 768px) {
          .sports_logo {
            margin-right: -10px;
          }
          .nav_bar {
            padding: 3px 0 6px 0;
          }
        }

        @media (max-width: 375px) {
          .header {
            height: 46px;
          }
          .header .logo {
            padding: 0px;
            padding-top: 5px;
            margin-left: -20px;
            margin-top: -38px;
            padding-left: 0px;
            padding-right: 0px;
            height: 20px;
            width: 80px;
          }

          .sports_logo {
            margin: auto;
            padding-left: 8px;
            margin-top: 3px;
          }

          .nav_bar {
            padding: 3px 0 4px 0;
            display: inline-block;
            position: relative;
            z-index: 99;
          }
          .nav_bar {
            padding: 3px 0 0 0;
          }
        }

        @media (max-width: 320px) {
          .nav_bar::before {
            right: -18px;
          }
          .nav_bar {
            padding: 3px 0 0px 0;
          }
        }
      `}</style>
      <header className="header">
        <div className="logo">
          <Link href="/">
            <a className="nav_bar navbar-brand">
              <amp-img
                src="https://www.sports.info/assets/images/logo_v2.svg"
                alt="logo"
                className="sports_logo"
                height="32"
                width="111"
              />
            </a>
          </Link>
        </div>
        <div className="wologo">
          <ul>
            {router.locales.map((locale) => (
              <li key={locale}>
                <Link href={router.asPath} locale={locale}>
                  <a>{locale}</a>
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </header>
    </>
  );
};

export default Header;
