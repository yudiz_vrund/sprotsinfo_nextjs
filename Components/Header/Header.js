/* eslint-disable @next/next/no-img-element */
import React from 'react';
import Link from 'next/link';
import styles from '../Header/Header.module.css';
import { useRouter } from 'next/router';

const Header = () => {
  let router = useRouter();
  return (
    <header className={styles.header}>
      <div className={styles.logo}>
        <Link href="/">
          <a className={`${styles.nav_bar} navbar-brand`}>
            <img
              src="https://www.sports.info/assets/images/logo_v2.svg"
              alt="logo"
              className={`${styles.sports_logo}`}
            />
          </a>
        </Link>
      </div>
      <div className={styles.wologo}>
        <ul>
          {router.locales.map((locale) => (
            <li key={locale}>
              <Link href={router.asPath} locale={locale}>
                <a>{locale}</a>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </header>
  );
};

export default Header;
