import React from 'react';
import Link from 'next/link';
import styles from '../Footer/Footer.module.css';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';

const Footer = () => {
  let router = useRouter();
  let { t } = useTranslation();

  return (
    <footer className={styles.footer}>
      <p>
        {t('common:copyright')} &copy; {t('common:title')} {t('common:year')}
      </p>
      <Link href="/about" passHref>
        <p>{t('common:about')}</p>
      </Link>
      <ul>
        {router.locales.map((locale) => (
          <li key={locale}>
            <Link href={router.asPath} locale={locale}>
              <a>{locale}</a>
            </Link>
          </li>
        ))}
      </ul>
    </footer>
  );
};

export default Footer;
