import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';

const Footer = () => {
  let router = useRouter();
  let { t } = useTranslation();

  return (
    <>
      <style jsx>{`
        .footer {
          margin: 0 0 0 0;
          text-align: center;
          background-color: black;
          padding: 10px;
          font-family: 'IBM Plex Sans', sans-serif;
        }

        .footer p {
          margin: 5px 0;
          color: white;
        }

        .footer a {
          color: lightskyblue;
          text-decoration: none;
        }

        .footer ul {
          list-style: none;
          text-align: center;
          margin-left: -40px;
        }

        .footer ul li {
          display: inline;
          padding: 10px;
          text-align: center;
        }
      `}</style>
      <footer className="footer">
        <p>
          {t('common:copyright')} &copy; {t('common:title')} {t('common:year')}
        </p>
        <Link href="/about" passHref>
          <a>{t('common:about')}</a>
        </Link>
        <ul>
          {router.locales.map((locale) => (
            <li key={locale}>
              <Link href={router.asPath} locale={locale}>
                <a>{locale}</a>
              </Link>
            </li>
          ))}
        </ul>
      </footer>
    </>
  );
};

export default Footer;
